import { useParams, useNavigate } from "react-router-dom";
import { useState, useContext } from "react";
import AdoptedPetContext from "./AdoptedPetContext";
import Modal from "./modal";
import { useQuery } from "@tanstack/react-query";
import ErrorBoundary from "./ErrorBoundary";
import Carousel from "./Carousel";
import fetchPet from "./fetchPet";

const Details = () => {
  const [showModal, setShowModal] = useState(false);
  const navigate = useNavigate(); // function to programmatically re-route someone. eg send them back to the home page
  const [_, setAdoptedPet] = useContext(AdoptedPetContext);
  const { id } = useParams();

  // useQuery will automatically cache the result of the fetched API.
  // if the key exists, it will return the cached result, otherwise it will call fetchPet to retrieve
  // from the API.
  const results = useQuery(["details", id], fetchPet);

  // if the query has not completed, return the loading pane
  // Otherwise render the animal details
  if (results.isLoading) {
    return (
      <div className="loading-pane">
        <h2 className="loader">💽</h2>
      </div>
    );
  }

  const pet = results.data.pets[0];

  return (
    <div className="details">
      <Carousel images={pet.images} />
      <div>
        <h1>{pet.name}</h1>
        <h2>
          {pet.animal} – {pet.breed} – {pet.city}, {pet.state}
        </h2>
        <button onClick={() => setShowModal(true)}>Adopt {pet.name}</button>
        <p>{pet.description}</p>
        {showModal ? (
          <Modal>
            <div>
              <h1>Would you like to adopt {pet.name}?</h1>
              <div className="buttons">
                <button
                  onClick={() => {
                    setAdoptedPet(pet);
                    navigate("/");
                  }}
                >
                  Yes!
                </button>
                <button onClick={() => setShowModal(false)}>Cancel</button>
              </div>
            </div>
          </Modal>
        ) : null}
      </div>
    </div>
  );
};

function DetailsErrorBoundary(props) {
  return (
    <ErrorBoundary>
      <Details {...props} />
    </ErrorBoundary>
  );
}

export default DetailsErrorBoundary;
